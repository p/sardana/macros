from sardana.macroserver.macro import Macro, Type
import taurus
import time
import CATS

class mount_HT_sample(Macro):
    """
    Macro to mount a sample from the hot tools
    """
    param_def = [ 
                  [ 'sample_number', Type.Integer, '1', 'hot tool sample number (1-10)']
                ]

#    TITLE = 'Grab FWHM Beam'
#    ICON = 'ellipse_shape.png'
   
    def run( self, sample_number ):
        super_dev = taurus.Device('bl13/eh/supervisor')
        diff_dev = taurus.Device('bl13/eh/diff')
        cats_dev = taurus.Device('bl13/eh/cats')

        if sample_number < 1 or sample_number > 10:
            self.error("mount_HT_sample ERROR: The sample should be an integer between 1 and 10. Aborting")
            return
            
        # turn on robot
        #CATS.doPowerOn()
        # Setup beamline for mounting
        super_dev.goTransferPhase()
        nmaxiter = 50
        niter = 1
        while niter<nmaxiter and ( not str( super_dev.CurrentPhase ).upper() == 'TRANSFER' or not CATS.is_cats_powered() ):
            time.sleep(0.1)
            #self.info( "mount_HT_sample INFO: "+str( super_dev.CurrentPhase ).upper() )
        if not str( super_dev.CurrentPhase ).upper() == 'TRANSFER':
            self.error ('mount_HT_sample ERROR: Cant go to transfer phaser. Aborting')
            return
       
        # If the current gripper is not EMBL, abort with warning message
        if diff_dev.SampleOnMagnet:
            self.error("mount_HT_sample ERROR: There is a sample on the magnet, do an unmount first")
            return
        if not CATS.is_cats_idle():
            self.error("mount_HT_sample ERROR: The sample changer is running a trajectory, please wait and try again when finished")
            return
            

        # Now mount
        mountpos = diff_dev.MountingPosition

        cmd = 'put_HT(2,100,%d,0,0,0,0,0,0,0,%f,%f,%f)' % (sample_number,mountpos[0],mountpos[1],mountpos[2])
        self.info( "mount_HT_sample INFO: sending command to cats_dev.send_op_cmd: %s" % cmd)
        ans = cats_dev.send_op_cmd(cmd)
        self.info( "mount_HT_sample INFO: the robot answered %s" % ans)

class unmount_HT_sample(Macro):
    """
    Macro to mount a sample from the hot tools
    """
    param_def = [ 
                ]

#    TITLE = 'Grab FWHM Beam'
#    ICON = 'ellipse_shape.png'
   
    def run( self ):
        super_dev = taurus.Device('bl13/eh/supervisor')
        diff_dev = taurus.Device('bl13/eh/diff')
        cats_dev = taurus.Device('bl13/eh/cats')
        
        # turn on robot
        CATS.doPowerOn()
        # Setup beamline for mounting
        super_dev.goTransferPhase()
        nmaxiter = 50
        niter = 1
        while niter<nmaxiter and ( not str( super_dev.CurrentPhase ).upper() == 'TRANSFER' or not CATS.is_cats_powered() ):
            time.sleep(0.1)
            #self.info( "unmount_HT_sample INFO: "+str( super_dev.CurrentPhase ).upper() )
        if not str( super_dev.CurrentPhase ).upper() == 'TRANSFER':
            self.error ('unmount_HT_sample ERROR: Cant go to transfer phaser. Aborting')
            return
       
        # If the current gripper is not EMBL, abort with warning message
        if not diff_dev.SampleOnMagnet:
            self.error("unmount_HT_sample ERROR: There is no sample on the magnet, no sample to unmount, aborting")
            return
        if not CATS.is_cats_idle():
            self.error("unmount_HT_sample ERROR: The sample changer is running a trajectory, please wait and try again when finished")
            return
        if CATS.get_sampleondiff_info(cats_dev)[3] != 100:
            self.error("unmount_HT_sample ERROR: the mounted sample is not a HT sample, unmount it first")
            return            

        # Now mount
        mountpos = diff_dev.MountingPosition
        #        new_cmd = cmd.replace('x',str(mountpos[0])).replace('y',str(mountpos[1])).replace('z',str(mountpos[2]))

        cmd = 'get_HT(2,100,0,0,0,0,0,0,0,0,%f,%f,%f)' % ( mountpos[0],mountpos[1],mountpos[2] )
        self.info( "unmount_HT_sample INFO: sending command to cats_dev.send_op_cmd: %s" % cmd)
        ans = cats_dev.send_op_cmd(cmd)
        self.info( "unmount_HT_sample INFO: the robot answered %s" % ans)
        
