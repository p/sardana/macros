from sardana.macroserver.macro import Macro, Type
import taurus
import PyTango
import time
from  bl13check import status

class cryoshutter_anneal(Macro):

    """
    This macro is used to stop the cryostream ln2 flow over the crystal for a few seconds.
    """

    param_def = [ 
                   [ 'settime', Type.Float, 4, 'Cryoshutter anneal time']
                ]
   
    def run(self, settime):
        self.epsf = taurus.Device('bl13/ct/eps-plc-01')        
        self.cats_dev = taurus.Device('bl13/eh/cats')

        if status.is_lima_running():
            raise Exception('CRYOSHUTTER ERROR: detector in collection state')

        if self.cats_dev['do_PRO5_IDL'].value != True:
            raise Exception('CRYOSHUTTER ERROR: CATS is not idle')
 
        self.info('CRYOSHUTTER: moving diffractometer to safe state')
        superdev = taurus.Device('bl13/eh/supervisor')
        superdev.gotransferphase()
        tries = 0
        maxtries = 300
        sleeptime = 0.2
        while superdev.currentphase.upper() != 'TRANSFER' and tries < maxtries:
            time.sleep(sleeptime)
            tries = tries + 1
           
        if tries >= maxtries: 
            self.error('CRYOSHUTTER ERROR: The ln2cover could not be closed')
            raise Exception('CRYOSHUTTER ERROR: The ln2cover could not be closed')
        
        
        self.info('CRYOSHUTTER: LN2 cover is closed')
        self.execMacro('act cryoshu in')
        self.info('CRYOSHUTTER: CRYOSHUTTER is IN')
        time.sleep (settime)
        self.execMacro('act cryoshu out')
        self.info('CRYOSHUTTER: CRYOSHUTTER is OUT')
        
        
#        self.execMacro('ln2shower_setflow %.1f' % washflow)
#        self.info('LN2SHOWER_WASH: Succesfully set the pump flow...')
        


