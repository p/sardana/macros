from sardana.macroserver.macro import Macro, Type
import taurus
import time
import CATS
#import PyTango
#from  bl13check import status

CATS_RESET_MOTION_SAFE_POINT = [260, 230] # X should be higher, Y should be lower than these values

class cats_recover_collision(Macro):
    '''
    Recover the collision

    Parameters:
            
    '''
    param_def = [ 
                ]
                
    def run(self):
        cats_dev = taurus.Device('bl13/eh/cats')
        
        tool = cats_dev.Tool
        lid1 = cats_dev.di_Lid1Open
        timedout = False
        lastcommand = cats_dev.LastCommandSent
        
        self.info('RECOVER_COLLISION: Checking the state of the pycats server')            
        if CATS.is_cats_DS_on(): self.info('RECOVER_COLLISION: Pycats server is ready')
        else:
            if CATS.dowait_cats_DS_on(5): # when the DS cannot be initialize, the timeout returned by the function is true
                raise Exception('RECOVER_COLLISION ERROR: Pycats is in ALARM. Restart the DS in Jive. Afterwards restart the diffractometer and supervisor DS and MXCube')
                
#        if tool == 'Double': #and 'put' in lastcommand:
        if tool == 'Double' or 'EMBL': #and 'put' in lastcommand:
            self.info('RECOVER_COLLISION: Power ON')
            response = cats_dev.powerOn()
            if not response == 'on':
                raise Exception('RECOVER_COLLISION ERROR: Robot cant do power ON')
            if cats_dev.Message == 'Manual brake control selected': 
                raise Exception('RECOVER_COLLISION ERROR: Robot cant do power ON, the manual brake selector is not in the zero position\nEnter in the hutch and fix it')
            time.sleep(1)
            
            catsposition = CATS.position_as_array()
            tooloverlid = CATS.tool_pos_on_which_lid()
            if not tooloverlid == -1 :
                self.warning('RECOVER_COLLISION WARNING: The tool is over lid %s, trying a safe directory' % tooloverlid)
                self.info('RECOVER_COLLISION: Doing safe trayectory')
#                response = cats_dev.safe(5)
                if tool == 'Double': response = cats_dev.safe(5) 
                if tool == 'EMBL': response = cats_dev.safe(2)                
                if not response == 'safe':
                    raise Exception('RECOVER_COLLISION ERROR: Robot cant do safe')
            elif catsposition[0] > CATS_RESET_MOTION_SAFE_POINT[0] and catsposition[1] < CATS_RESET_MOTION_SAFE_POINT[1] :
                self.warning('RECOVER_COLLISION WARNING: The tool is not over a lid.')
                self.warning('         Trying recovery using resetmotion followed by home')
                response = cats_dev.resetmotion()
                if not response == 'resetMotion':
                    raise Exception('RECOVER_COLLISION ERROR: Robot cant do resetmotion')
                time.sleep(1)
                
#                response = cats_dev.home(5)
                if tool == 'Double':response = cats_dev.home(5)
                if tool == 'EMBL': response = cats_dev.home(2)
                if not response == 'home':
                    raise Exception('RECOVER_COLLISION ERROR: Robot cant do home')
            else:
                msg = 'RECOVER_COLLISION ERROR: the robot is in a position that does not allow automatic recovery'
                msg += '     Move the robot manually or using the teach pendant to a safe position, eg close to home, or over the plate hotel'
                raise Exception(msg)

            timedout = CATS.wait_cats_idle_home(60) # wait for robot to finish  
            if timedout: raise Exception('cats_recover_collision ERROR: time out on safe/home command')
            self.info('RECOVER_COLLISION: CATS is idle and at home')
              
            self.info('RECOVER_COLLISION: Doing abort')
            response = cats_dev.abort() # is this really necessary???
            if not response == 'abort':
                raise Exception('RECOVER_COLLISION ERROR: Robot cant do abort')

            self.info('RECOVER_COLLISION: Opening/closing tool A')
            response = cats_dev.opentool()
            if not response == 'opentool':
                raise Exception('RECOVER_COLLISION ERROR: Robot cant do opentool')
            time.sleep(1)
            response = cats_dev.closetool()
            if not response == 'closetool':
                raise Exception('RECOVER_COLLISION ERROR: Robot cant do closetool')

            time.sleep(1)

            if tool == 'Double': 
                self.info('RECOVER_COLLISION: Closing tool B')
                response = cats_dev.closetool2()
                if not response == 'closetool2':
                    raise Exception('RECOVER_COLLISION ERROR: Robot cant do closetool2')
                time.sleep(1)
                
            # remember sample on diff to recover the info of sample on diff after clear_memory
            cassamnum, casnum, samnum, lidnum, som = CATS.get_sampleondiff_info(cats_dev)

            # Clear memory to clear sample information on tool, then recover the info of sample on diff
            self.info('RECOVER_COLLISION: Clearing memory')
            response = cats_dev.clear_memory()
            if not response == 'clear memory':
                raise Exception('RECOVER_COLLISION ERROR: Robot cant do clear_memory')
            time.sleep(1)

            #then recover the info of sample on diff
            if cats_dev['di_PRI4_SOM'].value:
                self.info('RECOVER_COLLISION: Setting sample info')
                response = cats_dev.setondiff( [ str(lidnum), str(samnum), '1'] ) # unipuck
                if not response == 'setdiffr':
                    raise Exception('RECOVER_COLLISION ERROR: Robot cant do setondiff')
            else: self.info('RECOVER_COLLISION: No sample on magnet') 
            
            
            self.info('RECOVER_COLLISION: Doing dryhome')
#            response = cats_dev.dryhome(5)
            if tool == 'Double': 
                response = cats_dev.dryhome(5)
                if not response == 'dryhome':
                    raise Exception('RECOVER_COLLISION ERROR: Robot cant do dryhome')
                time.sleep(1)
            
            if tool == 'EMBL': 
                response = cats_dev.dry(2) 
                if not response == 'dry':
                    raise Exception('RECOVER_COLLISION ERROR: Robot cant do dry')
                    time.sleep(1)
#            if not response == 'dryhome':
#                raise Exception('RECOVER_COLLISION ERROR: Robot cant do dryhome')
            time.sleep(2)   

            timedout = CATS.wait_cats_idle_home(120) # wait for robot to finish  
            if timedout: raise Exception('RECOVER_COLLISION ERROR: time out on dryhome command')
            self.info('RECOVER_COLLISION: Dryhome done. CATS is idle and at home')
                
            self.info('RECOVER_COLLISION: Doing toolcal')
#            response = cats_dev.toolcalibration(5)
            if tool == 'Double': response = cats_dev.toolcalibration(5)
            if tool == 'EMBL': response = cats_dev.toolcalibration(2)
            if not response == 'toolcal':
                raise Exception('RECOVER_COLLISION ERROR: Robot cant do toolcal')
            time.sleep(3)    

            timedout = CATS.wait_cats_idle_home(360) # wait for robot to finish  
            if timedout: raise Exception('RECOVER_COLLISION ERROR: time out on toolcal command')
            self.info('RECOVER_COLLISION: Toolcal done. CATS is idle and at home')
            
#            self.info('RECOVER_COLLISION: Going to soak')
#            self.info('RECOVER_COLLISION: Going to soak')
            if tool == 'Double': 
                self.info('RECOVER_COLLISION: Going to soak')
                response = cats_dev.soak( ['5', '2'] ) # This doesnt work!
#            response = cats_dev.soak( ['5'], ['2'] )
                if not response == 'soak':
                    raise Exception('RECOVER_COLLISION ERROR: Robot cant do soak')
                                      
                time.sleep(2)                

                timedout = CATS.wait_cats_idle_home(120) # wait for robot to finish  
                if timedout: raise Exception('cats_recover_collision ERROR: time out on soak command')
            self.info('RECOVER_COLLISION: CATS recovered successfully')
            
            time.sleep(2)                
                
        else: 
            self.warning('cats_recover_collision WARNING: tool is %s and lastcommandsent is %s' %
                            (tool,lastcommand))
            self.warning('    These values are not compatible with this macro, solve the collision manually')
            


