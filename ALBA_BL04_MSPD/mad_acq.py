import time
import math
import PyTango, taurus
from sardana.macroserver.macro import *
from sardana.macroserver.msexception import UnknownEnv
from macro_utils.macroutils import SoftShutterController
from sardana.macroserver.recorders.storage import SPEC_FileRecorder
from sardana.macroserver.macros.scan import ascanct

MNT_GRP = "mad"

class mad_ct(Macro, SoftShutterController):
    
    param_def = [["time", Type.Float, 1.0, "Acq time"]]

    def prepare(self, *args, **kwargs):  
        SoftShutterController.init(self)  
                 
    def run(self, *args, **kwargs):
        time = args[0]
        oldMntGrp = self.getEnv("ActiveMntGrp")
        self.setEnv("ActiveMntGrp", MNT_GRP)
            
        try:
            self._fsShutter = self.getEnv("_fsShutter")
        except UnknownEnv, e:
            self._fsShutter = 0
            
        try:
            if self._fsShutter == 1:
                SoftShutterController.openShutter(self)
            self.execMacro("ct", time)
        finally:
            self.setEnv("ActiveMntGrp", oldMntGrp)
            if self._fsShutter == 1:
                SoftShutterController.closeShutter(self)

class mad_ascan(Macro, SoftShutterController):
    
    param_def = [ ['motor',      Type.Moveable,  None, 'Moveable to move'],
              ['start_pos',  Type.Float,   None, 'Scan start position'],
              ['final_pos',  Type.Float,   None, 'Scan final position'],
              ['nr_interv',  Type.Integer, None, 'Number of scan intervals'],
              ['integ_time', Type.Float,   None, 'Integration time']
             ]
    
    def prepare(self, *args, **kwargs):  
        SoftShutterController.init(self)       
        
         
    def run(self, *args, **kwargs):
        mot = args[0]
        StartPos =  args[1]
        EndPos   =  args[2]
        Npts      = args[3]
        intTim    = args[4]      
        args = (mot.name,StartPos,EndPos,Npts,intTim)
        oldMntGrp = self.getEnv("ActiveMntGrp")
        self.setEnv("ActiveMntGrp", MNT_GRP)
        
        try:
            self._fsShutter = self.getEnv("_fsShutter")
        except UnknownEnv, e:
            self._fsShutter = 0
        
        try:
            if self._fsShutter == 1:
                SoftShutterController.openShutter(self)
            self.execMacro('ascan', *args)
        finally: 
            self.execMacro("senv", "ActiveMntGrp", oldMntGrp)
            if self._fsShutter == 1:
                SoftShutterController.closeShutter(self)

class _madscan(Macro, SoftShutterController):

    mntGrp = "mad_cs"
    #mntGrp = "mad_cs_test"
    motName = "pd_oc"
    #motName = 'dmot1'      
    posName = "oc"
    #Oct 2017 setting the attribute tg1.extraInitialDelayTime to 50microsec (empirical value)
    tg = 'tg1'    
    software_delay= 0.05 

    param_def = [["startPos", Type.Float, None, "Starting position"],
                 ["endPos", Type.Float, None, "Ending pos value"],
                 ["speed", Type.Float, None, "deg/min"],
                 ["integTime", Type.Float, -1.0, "Integration time"]]
    
    def prepare(self, *args, **kwargs):
        self.debug("prepare entering...")
        
        #Comprove if Power of Moveable is ON
        self.debug("ckecking power of motor...")
        mot = taurus.Device(self.motName)     
        stateMotor = mot['PowerOn'].value
        if stateMotor == False:
            raise Exception("The Motor %s  is POWER OFF!!!, please check it." 
                            % self.motName)


        self.bkpScanFile = self.getEnv("ScanFile")

        # ascanct ScanRecorder Backup
        self.bkpScanRecorder = None
        try:
            self.bkpScanRecorder = self.getEnv("ascanct.ScanRecorder")
        except UnknownEnv:
            pass
        self.bkpScanDir = self.getEnv("ScanDir")
        self.bkpScanID = self.getEnv("ScanID")
        
        #Request New directory and dir to save Results
        self.MadScanFile = self.getEnv("MadScanFile")
        self.MadScanRecorder = self.getEnv("MadScanRecorder")
        self.MadScanDir = self.getEnv("MadScanDir")
        self.MadScanID = self.getEnv("MadScanID")
        
        # #Set the specific environment for Mad experiments
        self.setEnv("ScanID", self.MadScanID)
        self.setEnv("ScanFile", self.MadScanFile)
        self.debug("Previous ScanRecorder: %s"%self.getEnv("MadScanRecorder"))
        self.setEnv("ascanct.ScanRecorder", self.MadScanRecorder)
        self.setEnv("ScanDir", self.MadScanDir)
        
        # Check if the trigger by position is activated
        try:
            self.extTrigger = self.getEnv("_TriggerByPosition")
        except:
            self.extTrigger = False

    def preConfigure(self):
        self.debug("preConfigure entering...")
        name = self.posName
        channel = PyTango.DeviceProxy(name)
        channel["ZIndexEnabled"] = False
        channel["PulsesPerRevolution"] = 144000000                
        channel["Sign"] = -1
    
    def preStart(self):
        self.debug("preStart entering...")
        tg = taurus.Device(self.tg)
        tg["extraInitialDelayTime"] = self.software_delay
        self.warning("Setting NI, trigger by position : %r"%self.extTrigger)
        tg['slave'] = self.extTrigger

    def run(self, startPos, finalPos, speed, integTime):
        #works only in positive direction

        moveable = self.getMoveable(self.motName)
        spu = moveable.step_per_unit

        #speed in seconds
        speed_in_seconds = speed/60.
        steps_per_sec = speed_in_seconds*spu
        #round
        steps_per_seconds_round = round(steps_per_sec)
        sar_vel_in_sec = steps_per_seconds_round / spu
        real_speed = sar_vel_in_sec * 60
        speed = real_speed
        self.debug("Real Speed: %f" % real_speed) 

        scanTime = ((finalPos - startPos) / speed) * 60
        scanTime = round(scanTime, 5)
        self.debug("Scan time: %f" % scanTime) 
        if integTime < 0:
             integTime = (0.0005 / speed) * 60
        self.debug("Integration time: %f" % integTime) 
        nrOfTriggers = int(math.ceil(scanTime / integTime))
        self.debug("NrOfTriggers: %f" % nrOfTriggers) 
        oldMntGrp = self.getEnv("ActiveMntGrp")
        self.setEnv("ActiveMntGrp", self.mntGrp)
        try:
            mot = taurus.Device(self.motName)

            self.debug('%r %r %r %r %r 0'%(moveable, startPos, finalPos, nrOfTriggers, integTime))
            ascanct_macro, _ = self.createMacro("ascanct", moveable,
                                                          startPos, finalPos, 
                                                          nrOfTriggers, 
                                                          integTime)

            ascanct_macro.hooks = [
                                   (self.preConfigure, ["pre-configuration"]),
                                   (self.preStart, ["pre-start"])
                                  ]
            
            command = "madscan "+str(startPos)+" "+str(finalPos)+" "+str(speed)\
                      + " " + str(integTime)
            ascanct_macro._gScan._env['title'] = command
            # zreszela 20170302
            try:
                self.runMacro(ascanct_macro)
            except Exception as e:
                self.warning("ascanct did not finished correctly, probably due to wrong positioning of pd_oc", exc_info=1)
                self.debug(e)
        finally:

            self.debug("Leaving of _madscan")
            self.setEnv("ActiveMntGrp", oldMntGrp)
            
            posChan = taurus.Device(self.posName)
            posChan.stop()
            
            try:
                self.warning("Setting NI, trigger by position to : False")
                tg = taurus.Device(self.tg)
                tg['slave'] = False
            except:
                pass

            self.setEnv("ScanFile", self.bkpScanFile)
            if self.bkpScanRecorder:
                self.debug('Restoring %s'%self.bkpScanRecorder)
                self.setEnv("ascanct.ScanRecorder", self.bkpScanRecorder)
            self.setEnv("ScanDir", self.bkpScanDir)
            self.setEnv("MadScanID", self.getEnv("ScanID") )
            self.setEnv('ascanct.ScanRecorder', None)	    
            self.setEnv("ScanID", self.bkpScanID)
            #time.sleep(3)
