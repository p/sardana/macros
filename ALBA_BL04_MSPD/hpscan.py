import PyTango
import time
from sardana.macroserver.macro import Macro, Type
from macro_utils.macroutils import SoftShutterController

MARCCD_DELAY = 0.27


class hpascan(Macro, SoftShutterController):
    """newfile [numor] [numor file] [ numor file directory]"""
    param_def = [ ['motor',      Type.Moveable,  None, 'Moveable to move'],
       		  ['start_pos',  Type.Float,   None, 'Scan start position'],
       		  ['final_pos',  Type.Float,   None, 'Scan final position'],
       		  ['nr_interv',  Type.Integer, None, 'Number of scan intervals'],
       		  ['integ_time', Type.Float,   None, 'Integration time']
    		 ]
    
    
    def prepare(self, *args, **kwargs):  
        SoftShutterController.init(self) 

    def run(self, *args, **kwargs):
        
        try:
            self._fsShutter = self.getEnv("_fsShutter")
        except UnknownEnv, e:
            self._fsShutter = 0        
        
        try:             
            mot = args[0]
            StartPos =  args[1]
            EndPos   =  args[2]
            Npts      = args[3]
            intTim    = args[4]      
            args = (mot.name,StartPos,EndPos,Npts,intTim)

            mot.move(StartPos)
            if self._fsShutter == 1:
                SoftShutterController.openShutter(self)
            #self.execMacro('fsopen') 
            wait = 0.5 # FF chnaged from 1. 27Oct2016
            self.info("sleep time %.2f " %wait)
            time.sleep(wait)# added to avoid lower count at 1st point because of fs opening 
            self.execMacro('ascan', *args)
        finally: 
            #self.execMacro('fsclose') 
            if self._fsShutter == 1:
                SoftShutterController.closeShutter(self)

class hpdscan(Macro):
    """newfile [numor] [numor file] [ numor file directory]"""
    param_def = [ ['motor',      Type.Moveable,  None, 'Moveable to move'],
       		  ['start_pos',  Type.Float,   None, 'Scan start position'],
       		  ['final_pos',  Type.Float,   None, 'Scan final position'],
       		  ['nr_interv',  Type.Integer, None, 'Number of scan intervals'],
       		  ['integ_time', Type.Float,   None, 'Integration time']
    		 ]
    
    def run(self, *args, **kwargs):
        
	try:
            mot = args[0]
            StartPos =  args[1]
            EndPos   =  args[2]
            Npts      = args[3]
            intTim    = args[4]

            currentPos = mot.read_attribute("position").value
            StartPos = currentPos + StartPos
            EndPos = currentPos + EndPos
            args = (mot.name,StartPos,EndPos,Npts,intTim)
    	    self.execMacro('hpascan', *args)
	finally:
            #To prevent Exceptions if the motor is finishing the movement
            while mot.state() != PyTango.DevState.ON:
                time.sleep(0.1)
                self.checkPoint()
            mot.move(currentPos)
